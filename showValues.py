import os
import time
from readMemory import get_value

type_values = ['s', 'f', 'f', 'f']
addresses = []

def read_addresses():
    
    global addresses
    
    # Open the file
    f = open('addresses.txt', 'r')

    # Clear the addresses
    addresses = []

    # Read all the file
    for address in f:
        if address != '\n':
            addresses.append(address.rstrip('\n'))

    # Close the file
    f.close()

# Show a Value
def show_value(type_value, address):
    
    value = get_value(type_value, address)
    time.sleep(0.5)

    return value

# Show all the Values
def show_values():

    read_addresses()

    for i, address in enumerate(addresses):
        print(show_value(type_values[i], address))

# Show all the values in a loop
def loop_show_values():
    if len(addresses) != 0:
        for _ in range(0, 100):
            show_values()
            input('Next read...')
    else:
        print('There are not addresses in the file, addresses.txt')
        print('Execute the function getValues.py to get them.')

if __name__=="__main__":
    loop_show_all_values()
