import os

import main
import getValues
from showValues import show_values

# Main
if __name__ == "__main__":

    end = False

    # You are in the Start (True) o the menu Selector (False)
    # By default you must be in menu selector
    in_start = False

    while end == False:

        getValues.main(in_start, 0)

        print()

        print('Addresses:')
        with open('addresses.txt') as f:
            print(f.read().replace('\n',' '))

        print()

        show_values()

        print()

        print('You must have:')
        print('\"1-1 Hello World\"')
        print('0 <> 100')
        print('~= -94')
        print('~= -154')

        print()

        print ('It`s good?')

        end = bool( input('True/False: ') )
        
        in_start = False

    os.system('clear')
    print('Starting learning\n')

    main.start_game() 
