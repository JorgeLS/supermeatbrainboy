from pwn import *
import time
from pynput.keyboard import Key

from main import Menu

# Pause Wait Pause
# param=menu - Object to move keys
# param=act - Number of the action
def pwp(menu, act):

    print(f'Start {act}')

    menu.pressKey(Key.esc)
    time.sleep(1)
    menu.pressKey(Key.esc)

    print(f'End {act}')

# Get map address
# param=r - Object to send commands to Scanmem
# param=menu - Object to move keys
# return - Value address
def getMap(r, menu):

    mapAddress = '-1'

    r.sendline('" 1-1')
    time.sleep(2)
    
    menu.next_map() 
    r.sendline('" 1-2')

    # Get the memory address
    r.sendline('list')
    r.recvuntil('[ 0]')
    mapAddress = r.recvuntil(',')[:-1].decode('utf-8').lstrip()

    # Return to 1-1
    menu.pressKey(Key.left)
   
    return mapAddress

# Get clock address
# param=r - Object to send commands to Scanmem
# param=menu - Object to move keys
# return - Value address
def getClock(r, menu):

    clockAddress = '-1'

    menu.enter_map()

    time.sleep(1.6)
    menu.pressKey(Key.esc)

    r.sendline('1.4..2.45')
    time.sleep(2)
    
    for i in range(4):
        pwp(menu, i + 1)
        r.sendline('>')
        time.sleep(0.75)
 
    time.sleep(1)
    r.sendline('=')

    # Get the memory address
    r.sendline('list')
    r.recvuntil('[ 0]')
    # r.recvuntil('[ 1]')
    clockAddress = r.recvuntil(',')[:-1].decode('utf-8').lstrip()

    # Back to the map
    menu.exit_map(inEsc=True)

    return clockAddress

# Get X address
# param=r - Object to send commands to Scanmem
# param=menu - Object to move keys
# return - Value address
def getX(r, menu):

    xAddress = '-1'

    menu.enter_map()

    print('Start 1')
    r.sendline('-230')
    time.sleep(2)
    print('End 1')
    
    print('Start 2')
    menu.pressedKey(Key.right, 1)
    r.sendline('>')
    time.sleep(0.5)
    print('End 2')

    print('Start 3')
    menu.pressedKey(Key.right, 1)
    r.sendline('>')
    time.sleep(0.5)
    print('End 3')

    print('Start 4')
    menu.pressedKey(Key.left, 1)
    r.sendline('<')
    time.sleep(0.5)
    print('End 4')
    
    # Get the memory address
    r.sendline('list')
    r.recvuntil('[ 0]')
    xAddress = r.recvuntil(',')[:-1].decode('utf-8').lstrip()

    # Back to the map
    menu.exit_map()

    return xAddress

# Get Y address
# param=r - Object to send commands to Scanmem
# param=menu - Object to move keys
# return - Value address
def getY(r, menu):

    yAddress = '-1'

    menu.enter_map()

    print('Start 1')
    r.sendline('-175..-172')
    time.sleep(2.5)
    print('End 1')
    
    print('Start 2')
    menu.pressedKey(Key.right, 1.25)
    r.sendline('-160..-150')
    time.sleep(0.75)
    print('End 2')

    print('Start 3')
    menu.pressedKey(Key.left, 1)
    r.sendline('<')
    time.sleep(0.75)
    print('End 3')
    
    # Get the memory address
    r.sendline('list')
    r.recvuntil('[ 0]')
    yAddress = r.recvuntil(',')[:-1].decode('utf-8').lstrip()
    
    # Back to the map
    menu.exit_map()

    return yAddress

# Get the value address in the file
# param=FILENAME - Path of the file 
# return - The addresses of the game values
def getValuesFile(FILENAME):

    f = open(FILENAME, 'r')

    mapAddress = f.readline().rstrip('\n')
    clockAddress = f.readline().rstrip('\n')
    xAddress = f.readline().rstrip('\n')
    yAddress = f.readline().rstrip('\n') 

    f.close()

    return mapAddress, clockAddress, xAddress, yAddress

# Main function
# param=inStart - False-In map selector True-InStartMenu 
# param=doValue - Value to choose 0-all, in order Map-Clock-X-Y
def main(inStart=False, doValue=0):
    
    FILENAME = 'addresses.txt'
    menu = Menu()
    
    # Type of data to scan
    data_type = {
        'float'  : 'option scan_data_type float32',
        'string' : 'option scan_data_type string'
    }
    
    # Addresses of the memory values
    mapAddress = 0 
    clockAddress = 0
    xAddress = 0
    yAddress = 0 
    
    # Get the pid
    pid = os.popen('pidof SuperMeatBoy').read()
    
    # Create a procedure calling scanmem
    r = process(['/usr/bin/scanmem', str(pid).rstrip('\n')]);
    
    # Change to the game
    menu.changeto_game()
    
    # If Start, go to map selector
    if inStart:
        print('In Start')
        menu.start()
        time.sleep(0.5)
        print('In Map Selector')
    
    # Map
    if doValue == 0 or doValue == 1:

        print('Start Map')

        r.sendline(data_type['string'])
        
        mapAddress = getMap (r, menu)

        print('End Map')
        print (mapAddress)
       
        if doValue == 0:
            r.sendline('reset')
            time.sleep(1)
            print()
    
    # Clock
    if doValue == 0 or doValue == 2:

        print('Start Clock')

        r.sendline(data_type['float'])
        
        clockAddress = getClock (r, menu)

        print('End Clock')
        print (clockAddress)
        
        if doValue == 0:
            r.sendline('reset')
            time.sleep(2)
            print()
   
    # X
    if doValue == 0 or doValue == 3:

        print('Start X')

        r.sendline(data_type['float'])

        xAddress = getX (r, menu)

        print ('End X')
        print (xAddress)
        
        if doValue == 0:
            r.sendline('reset')
            time.sleep(2)
            print()
    
    # Y
    if doValue == 0 or doValue == 4:

        print('Start Y')

        # r.sendline(data_type['float'])
        
        # yAddress = getY (r, menu)

        # There is something wrong with collecting Y, this is the easy way
        yAddress = xAddress[:-1] + '4'

        print('End Y')
        print (yAddress)
    
    # Comeback to console
    menu.changeto_terminal()

    # Get the file values
    mapFile, clockFile, xFile, yFile = getValuesFile(FILENAME)

    # Remove the file if exist
    if os.path.exists(FILENAME):
        os.remove(FILENAME)

    # Open the addresses file
    f = open(FILENAME, "wt")

    # Write the address in the file
    f.write((mapAddress if mapAddress != 0 else mapFile) + '\n')
    f.write((clockAddress if clockAddress != 0 else clockFile) + '\n')
    f.write((xAddress if xAddress != 0 else xFile) + '\n')
    if doValue != 3:
        f.write((yAddress if yAddress != 0 else yFile) + '\n')
    # If only get the X, get Y too
    else:
        xAddress = xAddress[:-1] + '4'
        f.write(xAddress + '\n')
    
    # Close the file
    f.close()

if __name__=='__main__':

    inStart = False
    doValue = 0

    # Get the inputs
    for i, arg in enumerate(sys.argv):
        if i == 1:
            inStart = False if arg == 'False' else True
        if i == 2:
            doValue = int(arg)
    
    main(inStart, doValue)
