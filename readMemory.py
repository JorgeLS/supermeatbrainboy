import sys
import os

def get_value(type_value, memory_address):

    value = os.popen('sudo gdb --batch -ex \'x/' +
            type_value +
            ' 0x' +
            memory_address + '\' ' +
            '-p `pidof SuperMeatBoy` 2>/dev/null').read().split(':\t')[1]

    if type_value == 'f':
        return float(value)
    else:
        return str(value).strip('\n')

# Main
if __name__ == "__main__":
   
    # Get the inputs
    for i, arg in enumerate(sys.argv):
        if i == 1:
            type_of_value = arg
        elif i == 2:
            memory_address = arg
             
    if type_of_value == 'float':
        type_of_value = 'f'
    elif type_of_value == 'string':
        type_of_value = 's'
    else:
        type_of_value = 'u'

    print(get_value(type_of_value, memory_address))
