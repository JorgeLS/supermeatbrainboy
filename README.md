<img src="https://gitlab.com/JorgeLS/supermeatbrainboy/-/raw/master/media/logo.png" width="450" height="200">

# SUPER MEAT BRAIN BOY

**Proyecto de fin de Grado Superior**
**Desarrollo de Aplicaciones Multiplataforma**

Este proyecto se basa en el uso del aprendizaje automático para pasarse
los niveles del juego **Super Meat Boy**.

Al ejecutar el programa, se verá como va aprendiendo a pasarse cada nivel.

El proyecto todavía está en progreso, aunque se puede probar y ver como
va aprendiendo.

## Requerimientos

Estar en algún sistema operativo GNU/Linux o alguna de sus distribuciones.

Instalar el juego [Super Meat Boy](https://store.steampowered.com/app/40800/Super_Meat_Boy/).

Instalar mínimo [Python](https://www.python.org/downloads/) 2.7 o superior.

Instalar [Pip](https://pypi.org/project/pip/).

Instalar todos los [requirements](https://gitlab.com/JorgeLS/supermeatbrainboy/-/blob/master/requirements.txt)
para python.

Instalar el programa [scanmem](https://github.com/scanmem/scanmem).

Instalar el programa [xdotool](https://blog.hostonnet.com/installation-of-xdotool-on-linux).

Instalar el programa [xargs](https://shapeshed.com/unix-xargs/), si no está instalado por defecto.

Poder acceder al superusuario de tu sistema operativo.

## Instalación

`git clone https://gitlab.com/JorgeLS/supermeatbrainboy.git`

Abrir el directorio creado.

Instalar los módulos de Python: `pip install -r requirements.txt`

Asegúrate de que estén todos los módulos actualizados. `pip check`

#### Opcional

Crear una [entorno virtual](https://www.youtube.com/watch?v=Kg1Yvry_Ydk)
e instalar los módulos allí.

## Ejecución

Si tienes un entorno virtual asegúrate de usarlo.

1. Ejecutamos el juego.
2. Vamos a la pantalla de selección de mapas del primer mundo.
3. Ir al primer nivel sin tener que seleccionarlo (como en la imagen).

<img src="https://gitlab.com/JorgeLS/supermeatbrainboy/-/raw/master/media/map_selector.png" width="400" height="230">

4. En la consola, ve donde está la carpeta del git.
5. Ejecuta el programa con `python __init.py__`.
6. Para leer las variables de memoria del juego se necesitará la clave del
usuario administrador, deberás ponerla cuando te la pida.

#### Tener en cuenta
- Al iniciar el programa no podrás ejecutar nada, ni moverte por el ordenador,
para que el programa se ejecute correctamente.
