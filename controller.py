import sys
import time
from pynput.keyboard import Key, Controller

from main import Keyboard

# Print the usage
def printUsage():
    print('''
    Usage
    python3 controller.py 00000

    Inputs
        1.- LEFT
        2.- RIGHT
        3.- MOVE
        4.- JUMP
        5.- WALLJUMP
    ''')

class Game(Keyboard):

    # Constructor
    def __init__(self):
        super().__init__()

    # Move the character for 0.5 second
    # param=way -> Direction to move
    def move(self, way):
        self.pressedKey(way, 0.5)

    # Jump
    # param=way -> Direction to move
    def jump(self, way):
        self.pressedKeys(way, Key.space, 0.5)
        time.sleep(0.20)

    # Run while do another action
    # param=actiona -> Action to do while running
    def run(self, actiona):
        # The real key is the shift but I change it,
        # .steam/steam/steamapps/common/SuperMeatBoy/button.cfg
        with self.keyboard.pressed('e'): 
            actiona
            #with self.keyboard.pressed(Key.right):
            #    time.sleep(0.5)

    # Do a walljump
    def walljump(self):
        self.keyboard.press(Key.space)
        time.sleep(0.40)
        self.keyboard.release(Key.space)
        time.sleep(0.10)

def executeAction():

    if inputs[0] == 1:
        movement = directions[0]
    elif inputs[1] == 1:
        movement = directions[1]

    if inputs[2] == 1:
        actions[0](movement)
    elif inputs[3] == 1:
        actions[1](movement)
    elif inputs[4] == 1:
        actions[2](movement)

# Main
if __name__ == "__main__":
   
    # Get the inputs
    for i, arg in enumerate(sys.argv):
        if i == 1:
            inputs = list(map(int, arg))
    
    # Check if the inputs are correct
    if inputs is None or len(inputs) != 5 or len(sys.argv) < 1:
        printUsage()
        exit()
        
    game = Game()

    global directions
    global action
    # inputs
    directions = [Key.left, Key.right]
    actions = [game.move, game.jump, game.walljump]

    executeAction()
