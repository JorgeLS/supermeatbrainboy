import time
import os

from pynput.keyboard import Key, Controller

# Class to use the keyboard
class Keyboard:

    # Constructor
    def __init__(self):
        self.keyboard = Controller()
    
    # Press a key
    # param=key - Key to press
    def pressKey(self, key):
        self.keyboard.press(key)
        # Must have this to using in a game
        time.sleep(0.15)                    
        self.keyboard.release(key)
        time.sleep(0.05)
        
    # Press two keys at the same time
    # param=key       - Main key to press
    # param=secondKey - Secondary key to press
    def pressKeys(self, key, secondKey):   
        with self.keyboard.pressed(secondKey):
            self.keyboard.press(key)
            self.keyboard.release(key)

    # Pressed a key
    # param=key - Key to press
    # param=t   - Key press time
    def pressedKey(self, key, t):
        with self.keyboard.pressed(key):
            time.sleep(t)
    
    # Pressed two keys at the same time
    # param=key       - Main key to press
    # param=secondKey - Secondary key to press
    # param=t         - Keys press time
    def pressedKeys(self, key, secondKey, t):
        with self.keyboard.pressed(secondKey):
            with self.keyboard.pressed(key):
                time.sleep(t)

class Menu(Keyboard):

    # Constructor
    def __init__ (self):
        super().__init__()
        self.terminal_name = os.popen('xdotool getactivewindow getwindowname').read().strip('\n')

    # Change the window with the command (ALT + TAB)
    def change_window(self):
        self.keyboard.press(Key.alt)
        self.keyboard.press(Key.tab)
        self.keyboard.release(Key.tab)
        self.keyboard.press(Key.tab)
        self.keyboard.release(Key.tab)
        self.keyboard.release(Key.alt)
        time.sleep(1.2)

    # Change focus to the window game
    def changeto_game(self):
        os.system('xdotool search --pid `pidof SuperMeatBoy` | xargs xdotool windowactivate')
    
    # Change focus to the window terminal
    def changeto_terminal(self):
        os.system('xdotool search --name \'{0}\' | xargs xdotool windowactivate'.format(self.terminal_name))
    
    # Goes from menu to map selector
    def start(self):
        self.pressKey(Key.space)
        time.sleep(0.5)
        self.pressKey(Key.space)
        time.sleep(1.75)
        self.pressKey(Key.space)
        time.sleep(4)
    
    # Goes to menu
    def exit(self):
        self.pressKey(Key.esc)
        self.pressKey(Key.up)
        self.pressKey(Key.space)
        time.sleep(0.5)
        self.pressKey(Key.space)

    # Enter in the map selected
    def enter_map(self):
        time.sleep(1.75)
        self.pressKey(Key.space)
        time.sleep(2.5)

    # Goes to the map selector
    def exit_map(self, inEsc=False):
        if inEsc == False:
            self.pressKey(Key.esc)
        self.pressKey(Key.down)
        self.pressKey(Key.down)
        self.pressKey(Key.space)
        time.sleep(0.70)
        self.pressKey(Key.space)

    # Goes to the next map
    def next_map(self):
        self.pressKey(Key.right)
        time.sleep(0.5)        
    
    # Reset the level
    def reset(self):
        print('Reset Level')
        self.exitmap()
        self.entermap()

    # Change the level when is finished
    def next_level(self):
        time.sleep(3.5)
        self.pressKey(Key.space)
        print('Next Level')
        time.sleep(1.5)
 
    # Goes to the indicated level
    # param=level -> Level to go
    def goto_level(self, level):
        for x in range(1, level):
            self.nextmap()
        self.entermap()
        self.runlevel(level)

    # Run the indicated level
    # param=level -> Level to go
    def run_level(self, level): 
        # Level 1_1
        if level == 1:
            print ('Level 1')
            if self.level1_1():
                self.nextlevel()
            else:
                print ('Level 1 finished')
 
        # Level 1_2
        elif level == 2:
            print ('Level 2')
            if self.level1_2():
                self.nextlevel()
            else:
                print ('Level 2 finished')

        # Level 1_3
        elif level == 3:
            print ('Level 3')
            if self.level1_3():
                self.nextlevel()
            else:
                print ('Level 3 finished')

        # Level 1_4
        elif level == 4:
            print ('Level 4')
            if self.level1_4():
                self.nextlevel()
            else:
                print ('Level 4 finished')

        # Level 1_5
        elif level == 5:
            print ('Level 5')
            if self.level1_5():
                self.nextlevel()
            else:
                print ('Level 5 finished')

        else:
            print ('Level selector ERROR')

    # Function to run the level
    def level1_1(self):
        print('Start learn Level 1')
        os.system('python brain/Learner.py 1_1')
        return False

    # Function to run the level
    def level1_2(self):
        print('Start learn Level 2')
        os.system('python brain/Learner.py 1_2')
        return False

    # Function to run the level
    def level1_3(self):
        # print('Start learn Level 3')
        # os.system('python brain/Learner.py 1_3')
        return False

    # Function to run the level
    def level1_4(self):
        # print('Start learn Level 4')
        # os.system('python brain/Learner.py 1_4')
        return False

    # Function to run the level
    def level1_5(self):
        # print('Start learn Level 5')
        # os.system('python brain/Learner.py 1_5')
        return False

# Start the main process
def start_game():

    # Object instantiation
    menu = Menu()
    
    # Change to the game window
    menu.changeto_game()
    
    # Go to the map selector
    # menu.start()

    # Run the levels
    # menu.goto_level(1)

    # Run a specific level
    menu.run_level(1)
    #menu.run_level(2)
    #menu.run_level(3)
    #menu.run_level(4)
    #menu.run_level(5)

    # Exit when finish
    menu.changeto_game()
    menu.exit_map()

    # Change to the console window 
    menu.changeto_terminal()
