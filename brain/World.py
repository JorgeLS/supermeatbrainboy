# The original author of this code
__author__ = 'philippe'
__github__ = 'https://github.com/llSourcell'

from tkinter import *
import sys
import time 
from pynput.keyboard import Key
sys.path.insert(0, '/home/georarth/work/supermeatbrainboy/')

import brain.Values as Values
import showValues
from controller import Game

master = Tk()
control = Game()

# Level, by default is Example
level = Values.level

# Max and Min Score
cell_score_min = -0.2
cell_score_max = 0.2

# Width of cells
Width = Values.Width
# Max/Min Positions of cells
posMax = Values.posMax
posMin = Values.posMin
# Possible actions, do it in order
actions = Values.actions

# Start Score
score = 2
# Walking reward
walk_reward = -0.04
restart = False

# Number of cells in X and Y 5x5
(x, y) = (Values.x, Values.y)
# Where start the player
playerStart = Values.playerStart
# Cell where it starts
player = playerStart

# Where is the walls or obstacles
walls = Values.walls
# Where is the killer blocks and the final points
specials = Values.specials 
# flags to limit the movements
flags = Values.flags

# Create the board
board = Canvas(master, width=x*Width, height=y*Width)

# All cells scores
cell_scores = {}

# Create the virtual and no virtual grid
def render_grid():
    global specials, walls, Width, x, y, player
    for i in range(x):
        for j in range(y):
            board.create_rectangle(i*Width, j*Width, (i+1)*Width, (j+1)*Width, fill="white", width=1)
            temp = {}
            cell_scores[(i,j)] = temp
    for (i, j, c, w) in specials:
        board.create_rectangle(i*Width, j*Width, (i+1)*Width, (j+1)*Width, fill=c, width=1)
    for (i, j) in walls:
        board.create_rectangle(i*Width, j*Width, (i+1)*Width, (j+1)*Width, fill="black", width=1)

render_grid()

# Create all the cell score
def set_cell_score(state, action, val):
    global cell_score_min, cell_score_max
    green_dec = int(min(255, max(0, (val - cell_score_min) * 255.0 / (cell_score_max - cell_score_min))))
    green = hex(green_dec)[2:]
    red = hex(255-green_dec)[2:]
    if len(red) == 1:
        red += "0"
    if len(green) == 1:
        green += "0"
    color = "#" + red + green + "00"

# Main movements function
# It needs a way to move
def try_move(new_x, new_y):
    global player, x, y, score, walk_reward, me, restart
    if restart == True:
        restart_game()
    score += walk_reward
    if (new_x >= 0) and (new_x < x) and (new_y >= 0) and (new_y < y) and not ((new_x, new_y) in walls):
        board.coords(me, new_x*Width+Width*2/10, new_y*Width+Width*2/10, new_x*Width+Width*8/10, new_y*Width+Width*8/10)
        player = (new_x, new_y)
    for (i, j, c, w) in specials:
        if new_x == i and new_y == j:
            score -= walk_reward
            score += w
            if score > 0:
                print ("Success! score: ", score)
            else:
                print ("Fail! score: ", score)
            restart = True
            return
    # print('score:', score)

# Return the cell 
def what_cell(posX, posY):

    # Get X
    for cell, X in enumerate(range(Values.posMin[0], Values.posMax[0], 15)):
        if posX >= X and posX < X + 15:
            posX = cell
            break

    # Get Y
    for cell, Y in enumerate(range(Values.posMin[1], Values.posMax[1], -15)):
        if posY < Y and posY >= Y - 15:
            posY = cell
            break

    return posX, posY

# Take the X and Y values from the game
def getXY():

    x = showValues.show_value(showValues.type_values[2], showValues.addresses[2])
    y = showValues.show_value(showValues.type_values[3], showValues.addresses[3])

    return x, y

# Actions to move and try to move
def call_move_right():
    # move
    control.move(Key.right)
    time.sleep(0.5)

    # Get the actual X and Y
    x, y = getXY()

    # Get the actual cell
    cellX, cellY = what_cell(x, y)

    # Try to move
    try_move(cellX, cellY)

def call_move_left():
    # move
    control.move(Key.left)
    time.sleep(0.5)

    # Get the actual X and Y
    x, y = getXY()

    # Get the actual cell
    cellX, cellY = what_cell(x, y)

    # Try to move
    try_move(cellX, cellY)

def call_jump_right():
    # move
    control.jump(Key.right)
    time.sleep(0.5)

    # Get the actual X and Y
    x, y = getXY()

    # Get the actual cell
    cellX, cellY = what_cell(x, y)

    # Try to move
    try_move(cellX, cellY)

def call_jump_left():
    # move
    control.jump(Key.left)
    time.sleep(0.5)

    # Get the actual X and Y
    x, y = getXY()

    # Get the actual cell
    cellX, cellY = what_cell(x, y)

    # Try to move
    try_move(cellX, cellY)

# Restart the game
def restart_game():
    global player, score, me, restart
    player = playerStart
    score = 1
    restart = False
    board.coords(me, player[0]*Width+Width*2/10, player[1]*Width+Width*2/10, player[0]*Width+Width*8/10, player[1]*Width+Width*8/10)

def has_restarted():
    return restart

# To draw the player
# me = board.create_rectangle(player[0]*Width+Width*2/10, player[1]*Width+Width*2/10,
                            # player[0]*Width+Width*8/10, player[1]*Width+Width*8/10, fill="orange", width=1, tag="me")
me = board.create_rectangle(player[0]*Width+Width*2/10, player[1]*Width+Width*2/10,
                            player[0]*Width+Width*8/10, player[1]*Width+Width*8/10, fill="red", width=1, tag="me")

board.grid(row=1, column=0)


def start_game():
    showValues.read_addresses()
    master.resizable(False, False)
    master.mainloop()
