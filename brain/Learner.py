# The original author of this code
__author__ = 'philippe'
__github__ = 'https://github.com/llSourcell'

import threading
import time
import ast

import World
from main import Menu

# GLOBAL VARIABLES

# Discount that will have the score
discount = 0.1
# All the posible actions
actions = World.actions
# States or posible actions/ways
states = []
# The percentage of each state with the possible actions 
Q = {}

for i in range(World.x):
    for j in range(World.y):
        states.append((i, j))

# Add the temp for each state and add in the Q
for state in states:
    temp = {}
    for action in actions:
        if World.level != 'Example' and state in World.flags:
            temp[action] = World.flags[state][action]
        else:
            temp[action] = 0.1
        World.set_cell_score(state, action, temp[action])
    Q[state] = temp

# Set the score in the special cells
for (i, j, c, w) in World.specials:
    for action in actions:
        Q[(i, j)][action] = w
        World.set_cell_score((i, j), action, w)

# FUNCTIONS

# Load the score of each cell
def load_run():
    global Q

    with open(f'brain/runs/run{World.level}.txt', 'r') as f:
        contents = f.read()

    Q = ast.literal_eval(contents)

# Save the score of each cell
def save_run():
    global Q

    with open(f'brain/runs/run{World.level}.txt', 'w') as f:
        print(Q, file=f)

# Give an action an call to do it
# param=action - Action to do
def do_action(action):
    s = World.player
    r = -World.score

    print(s, action)

    # UP
    if action == 'up':
        World.try_move(s[0], s[1]-1)
    # Down
    elif action == 'down':
        World.try_move(s[0], s[1]+1)
    # Left
    elif action == 'left':
        World.try_move(s[0]-1, s[1])
    # Right
    elif action == 'right':
        World.try_move(s[0]+1, s[1])
    # MoveRight/Up
    elif action == 'move_right':
        World.call_move_right()
    # MoveLeft/Down
    elif action == 'move_left':
        World.call_move_left()
    # JumpRight/Left
    elif action == 'jump_right':
        World.call_jump_right()
    # JumpRight/Right
    elif action == 'jump_left':
        World.call_jump_left()
    else:
        return

    s2 = World.player
    r += World.score

    return s, action, r, s2

# Choose the better action to move in a state
# param=s - State to get the maximum reward
def max_Q(s):
    val = None
    act = None
    for a, q in Q[s].items():
        if val is None or (q > val):
            val = q
            act = a

    return act, val

# Increase/Decrease the score of the cells in which the player passes
# param=s     - Actual state
# param=a     - Action to update
# param=alpha - Learning rate
# param=inc   - Value that increases the reward
def inc_Q(s, a, alpha, inc):
    Q[s][a] *= 1 - alpha
    Q[s][a] += alpha * inc
    # print(s, a, alpha, inc, Q[s])
    World.set_cell_score(s, a, Q[s][a])

# Main function to the action
def run():
    global discount
    menu = Menu()
    
    time.sleep(0.5)
    alpha = 1
    t = 1
    count = 0
    
    if World.level != 'Example':
        # Put the current position where the character is
        x, y = World.getXY()
        cellX, cellY = World.what_cell(x, y)
        World.try_move(cellX, cellY)

        # Change to the game and enter in the map
        menu.changeto_game()
        menu.enter_map()

    while count != 25:

        # The actual state
        s = World.player

        # Pick the action with the maximum reward
        max_act, max_val = max_Q(s)
        # Do the action and get the next state
        (s, a, r, s2) = do_action(max_act)

        # Pick the action with the maximum reward
        max_act, max_val = max_Q(s2)
        # Update Q
        inc_Q(s, a, alpha, r + discount * max_val)

        # Check if the player has reached a special cell
        t += 1.0
        if World.has_restarted():
            World.restart_game()
            time.sleep(0.01)
            t = 1.0
            count += 1
            if World.level != 'Example':
                time.sleep(2)
                World.control.pressKey('w')
                time.sleep(1)

        # Update the learning rate
        alpha = pow(t, -0.1)

        # MODIFY THIS SLEEP IF THE GAME IS GOING TOO FAST.
        time.sleep(0.1)

    # When finally, save the run
    save_run()

    # Finished the program 
    World.master.destroy()
    if World.level != 'Example':
        menu.changeto_terminal()

if __name__ == '__main__':
    
    # Create a thread to the function run
    t = threading.Thread(target=run)
    t.daemon = True
    t.start()
    World.start_game()
