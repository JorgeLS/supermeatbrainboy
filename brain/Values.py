import sys

level = ''
Width = 0
actions = []
(x, y) = (0, 0)
playerStart = (0, 0)
walls = []
specials = []
posMin = (0, 0)
posMax = (0, 0)
flags = {}

if len(sys.argv) > 1:
    # Chosen level
    level = sys.argv[1]

    # Example
    if level == 'Example':
        Width = 100
        actions = ['right', 'up', 'down', 'left']
        (x, y) = (5, 5)
        playerStart = (0, y-1)
        walls = [(1, 1), (1, 2), (2, 1), (2, 2)]
        specials = [(4, 1, "red", -1), (4, 0, "green", 1)]
        # The posMin and posMax are not used for this level
        # The flags are not used for this level
    
    # Level 1_1
    elif level == '1_1':
        Width = 15
        actions = ['move_right', 'move_left', 'jump_right', 'jump_left']
        (x, y) = (34, 10)
        playerStart = (2, y-1)
        walls = [
                # Bandage Girl Plataform
                (12, 2), (13, 2), (14, 2), (15, 2), (16, 2), (17, 2), (18, 2), (19, 2), (20, 2),
                (12, 3), (13, 3), (14, 3), (15, 3), (16, 3), (17, 3), (18, 3), (19, 3), (20, 3),
                (12, 4), (13, 4), (14, 4), (15, 4), (16, 4), (17, 4), (18, 4), (19, 4), (20, 4),
                (12, 5), (13, 5), (14, 5), (15, 5), (16, 5), (17, 5), (18, 5), (19, 5), (20, 5),
                # Right Plataform 
                (28, 6), (29, 6), (30, 6), (31, 6), (32, 6), (33, 6),
                (28, 7), (29, 7), (30, 7), (31, 7), (32, 7), (33, 7),
                (28, 8), (29, 8), (30, 8), (31, 8), (32, 8), (33, 8),
                (28, 9), (29, 9), (30, 9), (31, 9), (32, 9), (33, 9),
                # Floor
                ( 9, 9), (10, 9), (11, 9), (12, 9), (13, 9), (14, 9), (15, 9), (16, 9), (17, 9), (18, 9), (19, 9),
                (20, 9), (21, 9), (22, 9), (23, 9), (24, 9), (25, 9), (26, 9), (27, 9)
                ]
        specials = [(15, 1, "magenta", 1), (16, 1, "magenta", 1), (17, 1, "magenta", 1)]
        posMin = (-255,  -25)
        posMax = ( 255, -175)
        flags = {
                # Bandage Girl Plataform
                (12, 1): {'move_right': 1, 'move_left': 0.1, 'jump_right': 0.2, 'jump_left': 0.1},
                (13, 1): {'move_right': 1, 'move_left': 0.1, 'jump_right': 0.2, 'jump_left': 0.1},
                (14, 1): {'move_right': 1, 'move_left': 0.1, 'jump_right': 0.2, 'jump_left': 0.1},
                (18, 1): {'move_right': 0.1, 'move_left': 1, 'jump_right': 0.1, 'jump_left': 0.2},
                (19, 1): {'move_right': 0.1, 'move_left': 1, 'jump_right': 0.1, 'jump_left': 0.2},
                (20, 1): {'move_right': 0.1, 'move_left': 1, 'jump_right': 0.1, 'jump_left': 0.2},
                # Right Plataform
                (28, 5): {'move_right': 0.2, 'move_left': 0.1, 'jump_right': 0.1, 'jump_left': 1},
                (29, 5): {'move_right': 0.2, 'move_left': 0.1, 'jump_right': 0.1, 'jump_left': 1},
                (30, 5): {'move_right': 0.2, 'move_left': 0.1, 'jump_right': 0.1, 'jump_left': 1},
                (31, 5): {'move_right': 0.2, 'move_left': 1, 'jump_right': 0.1, 'jump_left': 0.1},
                (32, 5): {'move_right': 0.2, 'move_left': 1, 'jump_right': 0.1, 'jump_left': 0.1},
                (33, 5): {'move_right': 0.2, 'move_left': 1, 'jump_right': 0.1, 'jump_left': 0.1},
                }
    
    # Level 1_2
    elif level == '1_2':
        Width = 15
        actions = ['move_right', 'move_left', 'jump_right', 'jump_left', 'wall_jump']
        (x, y) = (36, 23)
        playerStart = (3, y-1)
        walls = [
                (1, 1), (1, 2), (2, 1), (2, 2),
                (1, 1), (1, 2), (2, 1), (2, 2),
                ]
        specials = [(17, 5, "magenta", 1)]
else:
    print('''
    The arguments are incorrect.

        python Learner.py (Level)
        
        Examples:
    
        python Learner.py Example
        python Learner.py 1_1
        ''')
    exit()
